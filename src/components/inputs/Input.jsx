import React from "react";

function Input({ value, placeholder, isInValid, onChange = (e) => null, errors = [], ...rest }) {
  return (
    <div>
      <input
        className={`form-control ${isInValid && "is-invalid"}`}
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        {...rest}
      />
      {errors.map((error, index) => (
        <div key={index} className="invalid-feedback" style={{ display: "block" }}>
          {error}
        </div>
      ))}
    </div>
  );
}

export default Input;
