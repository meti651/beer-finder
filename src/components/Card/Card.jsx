import React from "react";
import PropTypes from "prop-types";

function Card({ children, style, className, onClick = () => null }) {
  return (
    <div className={`card ${className}`} style={style} onClick={onClick}>
      <div className="card-body" style={{ maxHeight: "100%" }}>
        {children}
      </div>
    </div>
  );
}

Card.propTypes = {
  children: PropTypes.node,
};

export default Card;
