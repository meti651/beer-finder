import React from "react";
import Styles from './Button.module.scss';

function Button({ text, children, type, onClick = (e) => null, style, ...rest }) {
  return (
    <button
      type={type}
      onClick={onClick}
      id={Styles.btn}
      style={{ ...style }}
      className="btn"
      {...rest}
    >
      {text || children}
    </button>
  );
}

export default Button;
