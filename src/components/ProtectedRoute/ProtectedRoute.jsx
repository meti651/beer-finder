import React from "react";
import PropTypes from "prop-types";
import { Navigate, Outlet } from "react-router-dom";

function ProtectedRoute({ children, condition, redirectPath }) {
  if (!condition) {
    return <Navigate to={redirectPath} replace />;
  }

  return children ? children : <Outlet />;
}

ProtectedRoute.propTypes = {
  children: PropTypes.node,
  condition: PropTypes.bool,
  redirectPath: PropTypes.string,
  path: PropTypes.string,
};

export default ProtectedRoute;
