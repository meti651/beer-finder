import React from "react";
import PropTypes from "prop-types";
import { ReactComponent as Logo } from "../../media/grape_logo.svg";
import Styles from "./Header.module.scss";

function Header({ rightSide }) {
  return (
    <header className="d-flex justify-content-between align-items-center" id={Styles.header}>
      <div id={Styles.logo_wrapper} className="d-flex gap-1 align-items-center">
        <Logo className={Styles.logo} />
        <span>Beer finder</span>
      </div>
      <div className="col">{rightSide}</div>
    </header>
  );
}

Header.propTypes = {
  rightSide: PropTypes.node,
};

export default Header;
