import React from "react";
import PropTypes from "prop-types";
import Content from "./Content";
import Header from "./Header";

import Styles from './Layout.module.scss';

function Layout({ children, headerRightSide, style }) {
  return (
    <main style={{ position: "relative", ...style }}>
      <Header rightSide={headerRightSide} />
      <div id={Styles.content}>
        <Content children={children} />
      </div>
    </main>
  );
}

Layout.propTypes = {
  children: PropTypes.node,
  headerRightSide: PropTypes.node,
};

export default Layout;
