import axios from "axios";
import { BEER } from "../constants";

const getBeers = ({ abvGt = 0, abvLt = 100, name, page = 1, perPage = 20 }) => {
  const params = { abv_gt: abvGt, abv_lt: abvLt, page, per_page: perPage };
  if (name) {
    params["beer_name"] = name;
  }
  return axios.get(BEER.BEERS, { params });
};

const getBeer = (beerId) => axios.get(`${BEER.BEERS}/${beerId}`);

export const beerServices = {
  getBeers,
  getBeer,
};
