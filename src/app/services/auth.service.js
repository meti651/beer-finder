import axios from "axios";
import { AUTH, USER_COOKIE } from "../constants";
import { setCookie } from "../utility/cookie.utility";

const login = (_) => axios.get(AUTH.LOGIN);

const logout = () => setCookie(USER_COOKIE, "", -1);

export const authServices = {
  login,
  logout,
};
