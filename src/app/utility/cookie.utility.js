export const setCookie = (name, value, expDays) => {
  const date = new Date();
  date.setTime(date.getTime() + expDays * 24 * 60 * 60 * 1000);
  const expiresAt = `expires=${date.toUTCString()}`;
  document.cookie = `${name}=${JSON.stringify(value)}; ${expiresAt};`;
  return document.cookie;
};

export const getCookieValue = (name) => {
  const cookies = decodeURIComponent(document.cookie);
  const cookieArray = cookies.split('; ');
  const result = cookieArray.find((cookieString) => cookieString.includes(`${name}=`));
  if (!result) {
    return undefined;
  }
  return JSON.parse(result.substring(name.length + 1));
};