import { USER_COOKIE } from "../constants";
import { authServices } from "../services/auth.service";
import { setCookie } from "../utility/cookie.utility";
import { ERROR_OCCURRED, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT } from "./types";

const login = (username) => (dispatch) =>
  authServices.login(username).then(
    ({ data }) => {
      if (data.answer === "yes") {
        dispatch({ type: LOGIN_SUCCESS, payload: username });
        setCookie(USER_COOKIE, username, 30);
        return { resolved: true, data, username, passed: true };
      }
      dispatch({ type: LOGIN_FAILED });
      return { resolved: true, data, username, passed: false };
    },
    (error) => {
      dispatch({ type: ERROR_OCCURRED, payload: error });
      return { resolved: false, username, passed: false };
    }
  );

const logout = () => (dispatch) => {
  authServices.logout();

  dispatch({ type: LOGOUT });
};

export const authActions = {
  login,
  logout,
};
