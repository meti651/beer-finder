import { beerServices } from "../services/beer.service";
import {
  FETCHING_BEERS,
  FETCHING_BEERS_END,
  FETCHING_BEERS_START,
  GET_BEERS_FAILED,
  GET_BEERS_SUCCESS,
  GET_BEER_FAILED,
  GET_BEER_SUCCESS,
  PAGINATE_BEERS_FAILED,
  PAGINATE_BEERS_SUCCESS,
  SELECT_BEER,
} from "./types";

const getBeers =
  ({ abvGt, abvLt, name, page, perPage }) =>
  (dispatch) => {
    dispatch({ type: FETCHING_BEERS_START });
    return beerServices.getBeers({ abvGt, abvLt, name, page, perPage }).then(
      ({ status, data: beers }) => {
        if (status === 200) {
          dispatch({ type: GET_BEERS_SUCCESS, payload: beers, params: { abvGt, abvLt, name, page, perPage } });
        } else {
          dispatch({ type: GET_BEERS_FAILED, payload: beers, params: { abvGt, abvLt, name, page, perPage } });
        }
        dispatch({ type: FETCHING_BEERS_END });
        return { resolved: true, beers, params: { abvGt, abvLt, name, page, perPage } };
      },
      (err) => {
        dispatch({ type: GET_BEERS_FAILED, payload: err, params: { abvGt, abvLt, name, page, perPage } });
        dispatch({ type: FETCHING_BEERS_END });
        return { resolved: false, err, params: { abvGt, abvLt, name, page, perPage } };
      }
    );
  };

const paginateBeers = (config) => (dispatch) => {
  dispatch({ type: FETCHING_BEERS_START });
  return beerServices.getBeers(config).then(
    ({ status, data: beers }) => {
      if (status === 200) {
        dispatch({ type: PAGINATE_BEERS_SUCCESS, payload: beers, params: config });
      } else {
        dispatch({ type: PAGINATE_BEERS_FAILED, payload: beers, params: config });
      }
      dispatch({ type: FETCHING_BEERS_END });
      return { resolved: true, beers, params: config };
    },
    (err) => {
      dispatch({ type: PAGINATE_BEERS_FAILED, payload: err, params: config });
      dispatch({ type: FETCHING_BEERS_END });
      return { resolved: false, err, params: config };
    }
  );
};

const getBeer = (beerId) => (dispatch) => {
  return beerServices.getBeer(beerId).then(
    ({ status, data: [beer] }) => {
      if (status === 200) {
        dispatch({ type: GET_BEER_SUCCESS, payload: beer });
      } else {
        dispatch({ type: GET_BEERS_FAILED, payload: beer });
      }
      return { resolved: true, beer };
    },
    (err) => {
      dispatch({ type: GET_BEER_FAILED, payload: err });
      return { resolved: false, err };
    }
  );
};

const selectBeer = (beer) => ({ type: SELECT_BEER, payload: beer });

export const beerActions = {
  getBeers,
  getBeer,
  paginateBeers,
  selectBeer,
};
