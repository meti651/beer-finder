const AUTH_API_ROOT = 'https://yesno.wtf/api';

export const AUTH = {
  LOGIN: `${AUTH_API_ROOT}`
}

const BEER_API_ROOT = 'https://api.punkapi.com/v2';

export const BEER = {
  BEERS: `${BEER_API_ROOT}/beers`,
};