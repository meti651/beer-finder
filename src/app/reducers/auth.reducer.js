import { LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT } from "../actions/types";
import { USER_COOKIE } from "../constants";
import { getCookieValue } from "../utility/cookie.utility";

const initState = {
  isLoggedIn: false,
  username: "",
};

const currUser = getCookieValue(USER_COOKIE);

if (currUser) {
  initState.username = currUser;
  initState.isLoggedIn = true;
}

export function authReducer(state = initState, action) {
  const { type, payload } = action;
  switch (type) {
    case LOGIN_SUCCESS:
      return { isLoggedIn: true, username: payload };
    case LOGIN_FAILED:
      return { isLoggedIn: false, username: "" };
    case LOGOUT:
      return { isLoggedIn: false, username: "" };
    default:
      return state;
  }
}
