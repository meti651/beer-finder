import {
  FETCHING_BEERS_END,
  FETCHING_BEERS_START,
  GET_BEERS_FAILED,
  GET_BEERS_SUCCESS,
  GET_BEER_FAILED,
  GET_BEER_SUCCESS,
  PAGINATE_BEERS_FAILED,
  PAGINATE_BEERS_SUCCESS,
  SELECT_BEER,
} from "../actions/types";

const initState = {
  beers: [],
  selectedBeer: undefined,
  params: { abvGt: 0, abvLt: 100, name: "", page: 1, perPage: 20 },
  isLoadingBeers: false,
};

export function beerReducer(state = initState, action) {
  const { type, payload, params } = action;

  switch (type) {
    case FETCHING_BEERS_START:
      return { ...state, isLoadingBeers: true };
    case FETCHING_BEERS_END:
      return { ...state, isLoadingBeers: false };
    case GET_BEER_SUCCESS:
      return { ...state, selectedBeer: payload };
    case GET_BEER_FAILED:
      return state;
    case GET_BEERS_SUCCESS:
      return { ...state, beers: payload, params };
    case GET_BEERS_FAILED:
      return { ...state, params };
    case PAGINATE_BEERS_SUCCESS:
      return { ...state, beers: [...state.beers, ...payload], params };
    case PAGINATE_BEERS_FAILED:
      return { ...state, params };
    case SELECT_BEER:
      return {...state, selectedBeer: payload};
    default:
      return state;
  }
}
