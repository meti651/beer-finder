import { configureStore } from "@reduxjs/toolkit";
import { authReducer } from "./reducers";
import { beerReducer } from "./reducers";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    beers: beerReducer,
  },
});
