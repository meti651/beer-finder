import { useCallback, useEffect, useRef, useState } from "react";

export default function useFetch(fetchFunc, init, runAtRender) {
  const [data, setData] = useState(init);
  const [isLoading, setIsLoading] = useState(false);
  const isMounted = useRef(true);

  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    if (runAtRender) {
      fetching();
    }
  }, [runAtRender, fetching]);

  const fetching = useCallback(
    (config) => {
      setIsLoading(true);
      fetchFunc(config)
        .then((resp) => {
          if (isMounted.current) {
            setData(resp);
          }
        })
        .catch((err) => console.error(err))
        .finally(() => {
          if (isMounted.current) {
            setIsLoading(false);
          }
        });
    },
    [fetchFunc]
  );

  return [data, isLoading, fetching];
}
