import { useState, useEffect } from 'react';

export default function useWindowWidthChange() {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    const resizeCallback = () => setWindowWidth(window.innerWidth);

    window.addEventListener('resize', resizeCallback);
    resizeCallback();

    return () => window.removeEventListener('resize', resizeCallback);
  }, []);

  return windowWidth;
}