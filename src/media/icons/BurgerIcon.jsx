import React, { useState } from "react";
import Styles from "./BurgerIcon.module.scss";

function BurgerIcon({ isOpen = false, onClick = () => null }) {
  return (
    <div
      className={`${Styles.icon} ${isOpen && Styles.open}`}
      onClick={() => {
        onClick();
      }}
    >
      <div />
    </div>
  );
}

export default BurgerIcon;
