import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes, useNavigate } from "react-router-dom";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import BeerList from "./features/beerList/Beers";
import Login from "./features/login/Login";
import SingleBeer from "./features/singleBeer/SingleBeer";

function App() {
  const { isLoggedIn, username } = useSelector((state) => state.auth);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/beers">
          <Route
            path=""
            element={
              <ProtectedRoute condition={username.length > 0 && isLoggedIn} redirectPath="/login">
                <BeerList />
              </ProtectedRoute>
            }
          />
          <Route
            path=":beerId"
            element={
              <ProtectedRoute condition={username.length > 0 && isLoggedIn} redirectPath="/login">
                <SingleBeer />
              </ProtectedRoute>
            }
          />
        </Route>
        <Route
          path="/login"
          element={
            <ProtectedRoute condition={username.length < 1 || !isLoggedIn} redirectPath="/beers">
              <Login />
            </ProtectedRoute>
          }
        />
        <Route
          path="/"
          element={
            <ProtectedRoute condition={username.length < 1 || !isLoggedIn} redirectPath="/beers">
              <Login />
            </ProtectedRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
