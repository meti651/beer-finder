import React, { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { authActions } from "../../app/actions/auth.action";
import Button from "../../components/buttons/Button";
import Card from "../../components/Card";
import Input from "../../components/inputs/Input";

function LoginForm() {
  const [username, setUsername] = useState("");
  const [isUsernameInvalid, setIsUsernameInvalid] = useState(false);
  const [errorMessage, setErrorMessage] = useState([]);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const submitForm = useCallback(
    (e) => {
      e.preventDefault();
      setErrorMessage([]);
      dispatch(authActions.login(username)).then(({ passed }) => {
        if (passed) {
          navigate("/beers");
          return;
        }
        setErrorMessage((state) => [...state, "You are not allowed to log in, try again"]);
      });
    },
    [dispatch, username, navigate]
  );

  const handleUsernameChange = useCallback(({ target }) => {
    if (target.value.length > 16) {
      const error = "The username can contain a maximum number of 16 characters";
      setErrorMessage((state) => (state.includes(error) ? state : [...state, error]));
      return;
    }
    setUsername(target.value);
  }, []);

  const handleBlur = useCallback(() => {
    if (username.length < 1) {
      setIsUsernameInvalid(true);
      setErrorMessage((state) => [...state, "You need to type at least 1 character"]);
    } else setIsUsernameInvalid(false);
  }, [username]);

  return (
    <Card
      style={{
        maxWidth: "500px",
        width: "100%",
        padding: "1.5rem",
        borderRadius: "20px",
        boxShadow: "0 0 120px #0000004D",
      }}
    >
      <form onSubmit={submitForm} className="d-flex flex-column gap-3">
        <label htmlFor="#username" style={{ fontSize: "24px", fontWeight: "bold" }}>
          Login
        </label>
        <Input
          id="username"
          value={username}
          onChange={handleUsernameChange}
          onBlur={handleBlur}
          required
          isInValid={isUsernameInvalid}
          errors={errorMessage}
        />
        <Button type="submit" style={{ backgroundColor: "#5c328a", color: "#fff", alignSelf: "end" }}>
          Login
        </Button>
      </form>
    </Card>
  );
}

export default LoginForm;
