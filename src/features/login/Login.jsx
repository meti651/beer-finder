import React from "react";
import Layout from "../../components/Layout";
import LoginForm from "./LoginForm";

export default function Login() {
  return (
    <Layout>
      <div
        style={{
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          display: "grid",
          placeItems: "center",
          width: "100%",
          maxWidth: "500px",
        }}
      >
        <LoginForm />
      </div>
    </Layout>
  );
}
