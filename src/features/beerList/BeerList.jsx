import React from "react";
import BeerCard from "./BeerCard";

function List({ beers, isLoading }) {
  return (
    <div
      className="ps-3 pe-3 gap-3"
      style={{
        display: "grid",
        gridTemplateColumns: "repeat(auto-fill,minmax(300px, 1fr))",
      }}
    >
      {beers.map((beer) => (
        <div key={beer.id}>
          <BeerCard beer={beer} />
        </div>
      ))}
    </div>
  );
}

export default List;
