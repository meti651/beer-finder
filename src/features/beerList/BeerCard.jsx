import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { beerActions } from "../../app/actions";
import Card from "../../components/Card";

import Styles from "./BeerCard.module.scss";

function BeerCard({ beer: { name, abv, image_url, id, ...rest } }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  return (
    <Card
      className={Styles.container}
      onClick={() => {
        dispatch(beerActions.selectBeer({ name, abv, image_url, id, ...rest }));
        navigate(`/beers/${id}`);
      }}
    >
      <div className="d-flex flex-column align-items-center" style={{ height: "100%" }}>
        <div className={`col ${Styles.image_wrapper}`}>
          <img src={image_url} alt={name} />
        </div>
        <strong>{name}</strong>
        <div className="mt-2">ABV: {abv}%</div>
      </div>
    </Card>
  );
}

export default BeerCard;
