import React, { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { beerActions } from "../../app/actions";
import Button from "../../components/buttons/Button";
import Input from "../../components/inputs/Input";

import Styles from "./Filter.module.scss";

function Filter() {
  const { params } = useSelector((state) => state.beers);
  const dispatch = useDispatch();
  const [name, setName] = useState(params.name);
  const [abvGt, setAbvGt] = useState(params.abvGt);
  const [abvLt, setAbvLt] = useState(params.abvLt);

  const filter = useCallback(() => {
    dispatch(beerActions.getBeers({ abvGt, abvLt, name, page: 1, perPage: 20 }));
  }, [abvGt, abvLt, name, dispatch]);

  return (
    <div className="d-flex flex-wrap align-items-center gap-2 p-3" id={Styles.container}>
      <div className="d-flex flex-wrap align-items-center justify-content-between col">
        <div className="d-flex align-items-center" style={{ gap: "3px" }}>
          Name:{" "}
          <Input
            value={name}
            placeholder="Name"
            onChange={({ target }) => setName(target.value)}
            style={{ width: "232px" }}
          />
        </div>
        <div className="d-flex align-items-center" style={{ gap: "3px" }}>
          <div className="d-flex align-items-center" style={{ gap: "3px" }}>
            ABV:
            <Input
              value={abvGt}
              placeholder="0"
              onChange={({ target }) => {
                if (!Number.isNaN(Number(target.value))) setAbvGt(target.value);
              }}
              style={{ maxWidth: "100px" }}
            />
            %
          </div>
          -
          <div className="d-flex align-items-center" style={{ gap: "3px" }}>
            <Input
              placeholder="100"
              value={abvLt}
              onChange={({ target }) => {
                if (!Number.isNaN(Number(target.value))) setAbvLt(target.value);
              }}
              style={{ maxWidth: "100px" }}
            />
            %
          </div>
        </div>
      </div>
      <div>
        <Button type="button" onClick={() => filter()} style={{ backgroundColor: "#5c328a", color: "#fff" }}>
          Filter
        </Button>
      </div>
    </div>
  );
}

export default Filter;
