import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { beerActions } from "../../app/actions";
import Layout from "../../components/Layout";
import Filter from "./Filter";
import HeaderComp from "./HeaderComp";
import List from "./BeerList";
import { useNavigate } from "react-router-dom";

function Beers() {
  const { beers, params, isLoadingBeers, selectedBeer } = useSelector((state) => state.beers);
  const dispatch = useDispatch();
  const isThereMore = useRef(true);
  const navigate = useNavigate();

  const handleScroll = useCallback(async () => {
    const bottomScrollPos = window.document.body.clientHeight;
    const currentScrollPos = window.scrollY + window.innerHeight;
    if (bottomScrollPos - currentScrollPos < 1000 && !isLoadingBeers && isThereMore.current) {
      const { beers } = await dispatch(beerActions.paginateBeers({ ...params, page: params.page + 1 }));
      if (beers.length < 1) isThereMore.current = false;
    }
  }, [params, dispatch, isLoadingBeers]);

  useEffect(() => {
    if (isLoadingBeers) return;
    dispatch(beerActions.getBeers({ ...params, page: 1, perPage: 20 }));
  }, [dispatch]);

  useEffect(() => {
    isThereMore.current = true;
  }, [beers]);

  // useEffect(() => {
  //   if (!selectedBeer) return;
  //   navigate("/beers/" + selectedBeer.id);
  // }, [selectedBeer]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [handleScroll]);

  return (
    <Layout headerRightSide={<HeaderComp />}>
      <Filter />
      <List beers={beers} isLoading={isLoadingBeers} />
    </Layout>
  );
}

export default Beers;
