import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { authActions } from "../../app/actions";
import useWindowWidthChange from "../../app/hooks/useWindowWidthChange";
import Button from "../../components/buttons/Button";
import BurgerIcon from "../../media/icons/BurgerIcon";
import Styles from "./HeaderComp.module.scss";

function HeaderComp() {
  const windowWidth = useWindowWidthChange();
  const { username } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <>
      {windowWidth < 520 && (
        <div
          className="w-100 pe-3 d-flex align-items-center justify-content-end"
          style={{ backgroundColor: "#000", height: "42px" }}
        >
          <BurgerIcon isOpen={isMenuOpen} onClick={() => setIsMenuOpen((state) => !state)} />
        </div>
      )}
      <div
        className={`${windowWidth < 520 ? Styles.mobile : ""} ${
          isMenuOpen && windowWidth < 520 ? Styles.open : ""
        } d-flex align-items-center justify-content-end gap-2 me-2`}
        style={windowWidth < 520 && !isMenuOpen ? { display: "none" } : {}}
      >
        <span style={{ fontWeight: "bold", color: "#fff" }}>Hello, {username}!</span>
        <Button
          style={{ backgroundColor: "#98fc4d", color: "#000", fontWeight: "bold" }}
          onClick={() => dispatch(authActions.logout())}
        >
          Logout
        </Button>
      </div>
    </>
  );
}

export default HeaderComp;
