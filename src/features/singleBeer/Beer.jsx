import React from "react";

import Styles from "./Beer.module.scss";

function Beer({ beer }) {
  return (
    <div id={Styles.container}>
      <div id={Styles.name}>
        <h2>{beer.name}</h2>
        <h3>{beer.tagline}</h3>
      </div>
      <div id={Styles.content}>
        <div id={Styles.image_wrapper}>
          <img src={beer.image_url} alt={beer.name} />
        </div>
      </div>
      <div id={Styles.description}>
        <div>
          <h4>Description</h4>
          <span>{beer.description}</span>
        </div>
        <div className="d-flex gap-1 align-items-center">
          <h4>ABV:</h4>
          <span style={{ fontSize: "18px" }}>{beer.abv}%</span>
        </div>
        <div>
          <h4>Food pairings:</h4>
          <ul>
            {beer.food_pairing.map((pair) => (
              <li key={pair}>{pair}</li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Beer;
