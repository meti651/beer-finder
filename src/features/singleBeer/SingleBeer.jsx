import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { beerActions } from "../../app/actions";
import Layout from "../../components/Layout";
import HeaderComp from "../beerList/HeaderComp";
import Beer from "./Beer";

function SingleBeer() {
  const { beerId } = useParams();
  const { selectedBeer: beer } = useSelector((state) => state.beers);
  const dispatch = useDispatch();

  useEffect(() => {
    if (beer?.id === Number(beerId)) return;
    dispatch(beerActions.getBeer(beerId));
  }, [beerId, dispatch]);

  return <Layout headerRightSide={<HeaderComp />}>
    {beer ? <Beer beer={beer} /> : "Loading"}
    </Layout>;
}

export default SingleBeer;
